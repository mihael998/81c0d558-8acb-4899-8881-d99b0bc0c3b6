import config from '../config'

export class ServiceModel {
    static cities = [
        { city: "bergamo", id: "6542116" },
        { city: "milano", id: "3173435" },
        { city: "roma", id: "3169070" }
    ]
    static units = "metric"
    static baseUrl = config.baseUrl
}