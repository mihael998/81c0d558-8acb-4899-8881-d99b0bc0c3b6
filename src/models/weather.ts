export interface CurrentCitiesForecast {
    avgTemp: number;
    highestHumidity: {
        city: string;
        value: number;
    };
    highestTemp: {
        city: string;
        value: number;
    }
}
export interface ReturnedCurrentCitiesForecast {
    "cnt": number,
    list: [
        {
            "main": {

                "temp": number,
                "pressure": number,
                "humidity": number,
                "temp_min": number,
                "temp_max": number
            },
            dt?: number,
            id?: string,
            name?: string
        }
    ]
}
export interface WeatherForecast {
    temp: number;
    pressure: number;
    humidity: number;
    dt: number;
}