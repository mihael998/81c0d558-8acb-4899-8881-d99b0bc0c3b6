import axios from 'axios'
import config from '../config'
import { CurrentCitiesForecast, ReturnedCurrentCitiesForecast } from '../models/weather'
import { ServiceModel } from '../models/service-model'

export class WeatherService extends ServiceModel {
    async geCurrentForecastForCity(city) {
        try {
            let resp = await axios.get(WeatherService.baseUrl + "/weather?id=" + WeatherService.cities.find((obj) => { if (obj.city == city) return true }).id + "&appid=" + config.apiKey + "&units=" + WeatherService.units, {})
            return resp
        } catch (error) {
            console.log(error)
            throw (error)
        }
    }
    async geCurrentForecastForCities() {
        try {
            let resp = await axios.get(WeatherService.baseUrl + "/group?id=" + WeatherService.cities.map(val => { return val.id }).toString() + "&appid=" + config.apiKey + "&units=" + WeatherService.units, {})
            let currentCitiesForecast: CurrentCitiesForecast;
            let returnedData: ReturnedCurrentCitiesForecast = resp.data;
            let maxCityTemp: { city: string, value: number } = undefined;
            let maxCityHumidity: { city: string, value: number } = undefined;
            let avgTemp: number = 0;
            returnedData.list.forEach(val => {
                avgTemp += val.main.temp;
                if (maxCityHumidity == undefined || maxCityHumidity.value < val.main.humidity) {
                    maxCityHumidity = {
                        city: val.name,
                        value: val.main.humidity
                    }
                }
                if (maxCityTemp == undefined || maxCityTemp.value < val.main.temp) {
                    maxCityTemp = {
                        city: val.name,
                        value: val.main.temp
                    }
                }
            });
            currentCitiesForecast = {
                avgTemp: avgTemp / returnedData.cnt,
                highestHumidity: maxCityHumidity,
                highestTemp: maxCityTemp

            };
            return currentCitiesForecast
        } catch (error) {
            console.log(error)
            throw (error)
        }
    }
}
