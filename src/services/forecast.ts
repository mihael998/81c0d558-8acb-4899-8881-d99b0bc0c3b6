import axios from 'axios'
import config from '../config'
import { CurrentCitiesForecast, ReturnedCurrentCitiesForecast, WeatherForecast } from '../models/weather'
import { ServiceModel } from '../models/service-model'

export class ForecastService extends ServiceModel {
    async geFiveDayForecastForCity(city) {
        try {
            let resp = await axios.get(ForecastService.baseUrl + "/forecast?id=" + ForecastService.cities.find((obj) => { if (obj.city == city) return true }).id + "&appid=" + config.apiKey + "&units=" + ForecastService.units, {})
            let returnedData: ReturnedCurrentCitiesForecast = resp.data;
            let fiveDayForecast: Array<WeatherForecast> = new Array();
            returnedData.list.forEach(val => {
                fiveDayForecast.push({
                    dt: val.dt,
                    humidity: val.main.humidity,
                    pressure: val.main.pressure,
                    temp: val.main.temp
                })
            })
            return fiveDayForecast
        } catch (error) {
            console.log(error)
            throw (error)
        }
    }
}
