import { Router, Request, Response } from 'express'
import { celebrate, Joi } from 'celebrate'
import { WeatherService } from '../../services/weather'
const axios = require('axios');
const route = Router()

export default (app: Router) => {
  app.use('/weather/cities/', route)

  route.get("/:city", celebrate({
    params: Joi.object({
      city: Joi.string().trim().lowercase().valid(...WeatherService.cities.map(function (item) {
        return item['city']
      })).required()
    })
  }), async (req, res, next) => {
    const weatherService = new WeatherService();
    try {
      let resp = await weatherService.geCurrentForecastForCity(req.params.city);
      res.json({ status: res.statusCode, body: resp.data })
    } catch (error) {
      next(error);
    }

  })
  route.get("", async (req, res, next) => {
    const weatherService = new WeatherService();
    try {
      let resp = await weatherService.geCurrentForecastForCities();
      res.json({ status: res.statusCode, body: resp })
    } catch (error) {
      next(error);
    }

  })
};
