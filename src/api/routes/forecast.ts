import { Router, Request, Response } from 'express'
import { celebrate, Joi } from 'celebrate'
import { ForecastService } from '../../services/forecast'
const axios = require('axios');
const route = Router()

export default (app: Router) => {
  app.use('/forecast/cities/', route)

  route.get("/:city", celebrate({
    params: Joi.object({
      city: Joi.string().trim().lowercase().valid(...ForecastService.cities.map(function (item) {
        return item['city']
      })).required()
    })
  }), async (req, res, next) => {
    const weatherService = new ForecastService();
    try {
      let resp = await weatherService.geFiveDayForecastForCity(req.params.city);
      res.json({ status: res.statusCode, body: resp })
    } catch (error) {
      next(error);
    }

  })
};
