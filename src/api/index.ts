import { Router } from 'express';
import weather from './routes/weather';
import forecast from './routes/forecast';

// guaranteed to get dependencies
export default () => {
	const app = Router();
	weather(app);
	forecast(app);

	return app
}