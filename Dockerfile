FROM node:10

WORKDIR /usr/src/app

ENV API_KEY='704baac54187407b7046c3c8f0845935'
ENV API_BASE_URL='https://api.openweathermap.org/data/2.5'

ENV PORT=3000

ENV LOG_LEVEL='debug'


COPY package*.json ./

RUN npm ci --only=production

COPY . .

EXPOSE ${PORT}
CMD [ "npm","run", "prod" ]